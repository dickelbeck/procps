# Check for platform specific features and generate config.h

include( CheckIncludeFile )
include( CheckFunctionExists )
include( CheckLibraryExists )
include( CheckSymbolExists )
include( CheckIncludeFile )
include( CheckIncludeFiles )
include( CheckSymbolExists )
include( CheckCSourceCompiles )
include( CheckTypeSize )
#include( CheckCCompilerFlag )


check_include_file( stdio_ext.h HAVE_STDIO_EXT_H )
check_include_file( error.h     HAVE_ERROR_H )
check_include_files( "stdlib.h;stdarg.h;string.h;float.h" STDC_HEADERS )
check_include_file( stdlib.h HAVE_STDLIB_H )
check_include_file( dlfcn.h HAVE_DLFCN_H )
check_include_file( fcntl.h HAVE_FCNTL_H )
check_include_file( inttypes.h HAVE_INTTYPES_H )
check_include_file( langinfo.h HAVE_LANGINFO_H )
check_include_file( libintl.h HAVE_LIBINTL_H )
check_include_file( limits.h HAVE_LIMITS_H )
check_include_file( memory.h HAVE_MEMORY_H )
check_include_file( locale.h HAVE_LOCALE_H )
check_include_file( ncurses.h HAVE_NCURSES_H )
check_include_file( curses.h HAVE_CURSES_H )


# http://www.cmake.org/cmake/help/v3.0/module/CheckTypeSize.html
check_type_size( pid_t HAVE_PID_T )
#check_include_file( stdint.h HAVE_STDINT_H )
#check_include_file( sys/types.h HAVE_SYS_TYPES_H )

check_include_file( arpa/inet.h HAVE_ARPA_INET_H )
check_include_file( netinet/in.h HAVE_NETINET_IN_H )
check_include_file( string.h HAVE_STRING_H )
check_include_file( strings.h HAVE_STRINGS_H )
check_include_file( sys/file.h HAVE_SYS_FILE_H )
check_include_file( sys/ioctl.h HAVE_SYS_IOCTL_H )
check_include_file( sys/param.h HAVE_SYS_PARAM_H )
check_include_file( sys/stat.h HAVE_SYS_STAT_H )
check_include_file( sys/time.h HAVE_SYS_TIME_H )
check_include_file( termios.h HAVE_TERMIOS_H )
check_include_file( unistd.h HAVE_UNISTD_H )
check_include_file( utmp.h HAVE_UTMP_H )
check_include_file( values.h HAVE_VALUES_H )
check_include_file( vfork.h HAVE_VFORK_H )
check_include_file( wchar.h HAVE_WCHAR_H )
check_include_file( wctype.h HAVE_WCTYPE_H )


check_function_exists( atexit HAVE_ATEXIT )
check_function_exists( alarm HAVE_ALARM )
check_function_exists( stat _POSIX_SOURCE )
check_function_exists( dcgettext HAVE_DCGETTEXT )
check_function_exists( dup2 HAVE_DUP2 )
check_function_exists( fork HAVE_FORK )
check_function_exists( getpagesize HAVE_GETPAGESIZE )
check_function_exists( gettext HAVE_GETTEXT )
check_function_exists( gettimeofday HAVE_GETTIMEOFDAY )
check_function_exists( iconv HAVE_ICONV )
check_function_exists( iswprint HAVE_ISWPRINT )
check_function_exists( malloc HAVE_MALLOC )
check_function_exists( memchr HAVE_MEMCHR )
check_function_exists( memmove HAVE_MEMMOVE )
check_function_exists( memset HAVE_MEMSET )
check_function_exists( mbrtowc HAVE_MBRTOWC )
check_function_exists( mmap HAVE_MMAP )
check_function_exists( munmap HAVE_MUNMAP )
check_function_exists( nl_langinfo HAVE_NL_LANGINFO )
check_function_exists( putenv HAVE_PUTENV )
check_function_exists( realloc HAVE_REALLOC )
check_function_exists( regcomp HAVE_REGCOMP )
check_function_exists( rpmatch HAVE_RPMATCH )
check_function_exists( select HAVE_SELECT )
check_function_exists( setlocale HAVE_SETLOCALE )
check_function_exists( strcasecmp HAVE_STRCASECMP )
check_function_exists( strchr HAVE_STRCHR )
check_function_exists( strcspn HAVE_STRCSPN )
check_function_exists( strpbrk HAVE_STRPBRK )
check_function_exists( strrchr HAVE_STRRCHR )
check_function_exists( strspn HAVE_STRSPN )
check_function_exists( strdup HAVE_STRDUP )
check_function_exists( strerror HAVE_STRERROR )
check_function_exists( strncasecmp HAVE_STRNCASECMP )
check_function_exists( strstr HAVE_STRSTR )
check_function_exists( strtol HAVE_STRTOL )
check_function_exists( strtoul HAVE_STRTOUL )
check_function_exists( strtoull HAVE_STRTOULL )
check_function_exists( strverscmp HAVE_STRVERSCMP )
check_function_exists( uname HAVE_UNAME )
check_function_exists( utmpname HAVE_UTMPNAME )
check_function_exists( vfork HAVE_VFORK )
check_function_exists( wcwidth HAVE_WCWIDTH )
check_function_exists( __fpending HAVE___FPENDING )


set( major_in_sysmacros FALSE )
set( minor_in_sysmacros FALSE )
set( makedev_in_sysmacros FALSE )
check_symbol_exists( major sys/sysmacros.h major_in_sysmacros )
check_symbol_exists( minor sys/sysmacros.h minor_in_sysmacros )
check_symbol_exists( makedev sys/sysmacros.h makedev_in_sysmacros )
if( ${major_in_sysmacros} AND ${minor_in_sysmacros} AND ${makedev_in_sysmacros} )
    set( MAJOR_IN_SYSMACROS TRUE )
    message( STATUS "MAJOR_IN_SYSMACROS:${MAJOR_IN_SYSMACROS}" )
endif()

set( major_in_mkdev FALSE )
set( minor_in_mkdev FALSE )
set( makedev_in_mkdev FALSE )
check_symbol_exists( major mkdev.h major_in_mkdev )
check_symbol_exists( minor mkdev.h minor_in_mkdev )
check_symbol_exists( makedev mkdev.h makedev_in_mkdev )
if( ${major_in_mkdev} AND ${minor_in_mkdev} AND ${makedev_in_mkdev} )
    set( MAJOR_IN_MKDEV TRUE )
    message( STATUS "MAJOR_IN_MKDEV:${MAJOR_IN_MKDEV}" )
endif()

check_c_source_compiles(
    "#include <errno.h>\nextern char *program_invocation_name;\nint main()\n{ program_invocation_name = \"test\"; return 0;}\n"
    HAVE_PROGRAM_INVOCATION_NAME )

check_c_source_compiles(
    "#include <errno.h>\nextern char *program_invocation_short_name;\nint main()\n{ program_invocation_short_name = \"test\"; return 0;}\n"
    HAVE_PROGRAM_INVOCATION_SHORT_NAME )


# Generate config.h in the build dir so that out of tree builds are the norm.
configure_file( ${PROJECT_SOURCE_DIR}/cmake_stuff/config.h.cmake  ${CMAKE_BINARY_DIR}/config.h )
